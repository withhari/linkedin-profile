var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');]

function builtwith(link) {
  link = link.replace('www.', '');
  if (link.indexOf('://') > 0) {
    link = link.split('://')[1]
  }
  request.get({
    url: 'https://builtwith.com/' + link,
    jar: true,
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Host': 'builtwith.com',
      'Referer': 'https://builtwith.com/',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0'
    }
  }, (e, h, b) => {
    if (e) {
      return console.log(e);
    }
    var $ = cheerio.load(b);
    var titles = [];
    $('li.active > span').each((i, li) => {
      titles.push($(li).text());
    });
    var techs = [];
    for (var i = 0; i < titles.length; i++) {
      var html = b.split('<span>'+titles[i]+'</span>')[1];
      if (i != titles.length - 1) {
        html = html.split('<span>'+titles[i + 1]+'</span>')[0];
      }
      var $$ = cheerio.load(html);
      $$('.techItem').each((i, div) => {
        var content = [];
        $$($$(this) + ' h3').each((n, h) => {
          if (content.indexOf($$(h).text()) === -1)
            content.push($$(h).text());
        });
        if (techs.indexOf(content.join(',')) === -1)
          techs.push(content.join(','));
      });
    }
    var OutPut = [];
    for (var i = 0; i < titles.length; i++) {
      OutPut.push(titles[i] + ',' + techs[i]);
    }
    fs.writeFile(link.replace('.', '') + '.csv', OutPut.join('\n'), function (err) {
      if (err) {
        console.log(err);
      }
    });
  });
}

// var links = ['http://microsoft.com', 'https://www.facebook.com', 'www.google.com', 'ea.com'];

// async.all(links, builtwith, function() {
//   console.log('done');
// })
module.exports = builtwith;