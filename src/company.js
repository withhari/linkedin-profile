var login = require('./in-login');
var fs = require('fs');

function visitCompany(nightmare, link, callback) {
  nightmare
    .goto(link)
    .wait('h1')
    .evaluate(() => {
      var company = {};
      company.name = document.querySelector('h1').textContent.trim();
      company.location = document.querySelector('.org-top-card-module__location') ? document.querySelector('.org-top-card-module__location').textContent.trim() : 'Not found';
      company.employee = document.querySelector('.snackbar-description-see-all-link') ? document.querySelector('.snackbar-description-see-all-link').textContent.split('See all')[1].trim() : 'Not found';
      company.website = document.querySelector('.org-about-company-module__company-page-url a') ? document.querySelector('.org-about-company-module__company-page-url a').href : 'Not found';
      company.see = document.querySelector('.snackbar-description-see-all-link') ? document.querySelector('.snackbar-description-see-all-link').href : 'Not found';
      company.followers = document.querySelector('.org-top-card-module__followers-count') ? document.querySelector('.org-top-card-module__followers-count').textContent.split('Company Followers')[1].trim() : 'Not found';
      company.industry = document.querySelector('.org-about-company-module__industry') ? document.querySelector('.org-about-company-module__industry').textContent.trim() : 'Not found'
      return company;
    })
    .then(company => {
      callback(null, company);
    })
    .catch(err => {
      if (err) {
        console.log('Error');
        console.log(err);
        callback(err, null);
      }
    });
}

module.exports = visitCompany;