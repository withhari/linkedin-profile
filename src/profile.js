function gotoProfiles(nightmare, profiles, oneDoneCallback, completedCallback) {
  var count = 0;
  var data = [];
  var total = profiles.length;

  function updateCount(profile) {
    count++;
    if (arguments.length >= 2) {
      //see if there's any error messages
      console.log(arguments[1]);
    }
    profile.data = profile.data || null;
    data.push(profile);
    oneDoneCallback(profile);
    if (count >= total) {
      endInstance();
    } else {
      setTimeout(gotoNext, 5000);
    }
  }

  function endInstance() {
    completedCallback(profiles, data);
  }

  function gotoNext() {
    if (profiles.length == 0) {
      return updateCount({
        index: -1
      });
    }
    var profile = profiles.pop();
    if (profile.link == null || profile.link.length == 0) {
      console.log('NULL');
      return updateCount(profile);
    }
    console.log('going to:' + profile.link);
    nightmare
      .goto(profile.link)
      .wait('h1')
      .scrollTo(0, 500)
      .evaluate(() => {
        var profileInfo = {}
        profileInfo.name = document.querySelector('h1').textContent.trim();
        var h3Nodes = document.querySelectorAll('h3');
        profileInfo.company = (h3Nodes.length >= 1 && h3Nodes[0] && h3Nodes[0].textContent) ? h3Nodes[0].textContent.trim() : null;
        profileInfo.country = (h3Nodes.length >= 3 && h3Nodes[2] && h3Nodes[2].textContent) ? h3Nodes[2].textContent.trim() : null;
        profileInfo.connectionCount = (h3Nodes.length >= 4 && h3Nodes[3] && h3Nodes[3].textContent) ? h3Nodes[3].textContent.trim() : null;
        if (profileInfo.connectionCount != null) {
          profileInfo.connectionCount = profileInfo.connectionCount.split(/\s/g)[0];
        }
        return profileInfo;
      })
      .then(profileInfo => {
        profile.data = profileInfo;
        updateCount(profile);
      })
      .catch(err => {
        if (err) {
          profile.data = null;
          console.log(err);
          profiles.push(profile);
          endInstance();
          //updateCount(profile, 'Calling after Error.');
        }
      })
  }
}

module.exports = gotoProfiles;